A very basic Drupal site for testing and proof of concept kind of things.

To deploy this codebase import db.sql.gz into a MySQL database and point enter
appropriate database details into settings.php.

Do not use this for production site, this repo directly includes the database,
unless all your data are public.

The codebase is developed on PHP 5.6 should work with PHP 5.4 or higher.
